#!/bin/sh -e

PROJECT_DIR="$( realpath $(dirname "$0")/.. )"

usage() {
cat << END_HELP
USAGE

    test.sh [-h|--help|help|unit|lint|all]

EXAMPLES

    # Print this message
    test.sh help

    # Run all tests
    test.sh
    test.sh all

    # Run unit tests
    test.sh unit

    # Run lint tests
    test.sh lint

    # Run unit and lint tests
    test.sh unit lint

END_HELP
}

TESTS="${@}"
INITIAL_DIR=${PWD}

flop() {
    cd "${INITIAL_DIR}" > /dev/null
    exit 1
}

for t in ${TESTS}; do
    if [ ${t} = '-h' ] || [ ${t} = '--help' ] || [ ${t} = 'help' ]; then
        usage
        exit 0
    fi
done

if [ -z "${TESTS}" ] || [ "${TESTS}" = 'all' ]; then
    TESTS='unit lint'
fi

for t in ${TESTS}; do
    if [ ${t} != 'unit' ] && [ ${t} != 'lint' ] && [ ${t} != 'api' ]; then
        echo "Unrecognized option '${t}'. Try 'test.sh help'." 1>&2
        exit 1
    fi
done

# Problem with pip install --editable for recent versions of pip
# https://github.com/microsoft/vscode-python/issues/14570
export PYTHONPATH="${PYTHONPATH}:${PROJECT_DIR}/test_lib"

cd "${PROJECT_DIR}" > /dev/null
for t in ${TESTS}; do
    if [ ${t} = 'unit' ]; then
        echo "Running unit tests..."
        pytest \
            --cov=src/file_data \
            --cov-report= \
            src/file_data \
            || flop
        coverage report \
            --fail-under=100 \
            --skip-covered \
            --show-missing \
            || flop
    elif [ ${t} = 'lint' ]; then
        echo "Running lint tests..."
        pylint \
            --rcfile .pylintrc \
            ${PROJECT_DIR}/src \
            || flop
    fi
done
cd "${INITIAL_DIR}" > /dev/null
