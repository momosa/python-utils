# Test Lib

Tools for unit and API tests.

## Overview

### Unit tests

Unit test tools to streamline mocking.
The primary advantage is that namespace specification
is consolidated cleanly in the `setUp` function of `TestUnit`.
Namespace specifications tend to involve a lot of redundant boilerplate.
They can also confuse newer folks.
The consolidation makes tests more readable,
and the way namespaces are specified makes
that specification easier to do correctly.

See the unit tests for `find_data` and other tools for examples.

### API tests

API test tools to simplify API tests.
To create a new set of API tests,
you need two files.
There's a `test_my_endpoint.py` file
in which you specify a data file where the tests are.
The data file is a `json` file in which you can specify
a set of tests.

The dir structure looks like:

```
api_tests
├── __init__.py
├── data
│   └── my_endpoint.json
└── test_my_endpoint.py
```

Here are example files:

```python
"""
test_my_endpoint.py

API integration tests against /my/endpoint/
"""

from test_lib.api import ApiTestCase


#pylint: disable=too-few-public-methods
class TestSearch(ApiTestCase):
    """
    Tests for the search API endpoints
    """


    def setUp(self):
        self.init_db("my_endpoint.json")


    def test_search(self):
        """
        Run API tests for /my_endpoint/
        """
        self.run_all()
```

```json
{

    "smoke": {
        "request_kwargs": {
            "url": "healthcheck/"
        },
        "method": "GET",
        "expected": {
            "status_code": 200
        }
    },

    "POST basic info": {
        "request_kwargs": {
            "url": "api/v1/category/vax_type/"
            "json": {
                "request": "data"
            }
        },
        "method": "POST",
        "expected": {
            "status_code": 200,
            "json": {
                "response": "data"
            }
        }
    },

    "GET bad data": {
        "request_kwargs": {
            "url": "my/endpoint/?bad=variable"
        },
        "method": "GET",
        "expected": {
            "status_code": 400
        }
    }

}
```

It's pretty intuitive to generate a set of tests
various response codes, response data, etc.

Note the `smoke` test.
Tests named `smoke` will always be run first,
and will periodically retry for up to 60s.
That gives the server time to boot up.

You can see real examples
[here][categorical_syllogisms_api_tests].

## Running tests

See `bin/test.sh` for examples.

The test lib works with standard test runners, like `pytest`.

This test lib was originally designed to be installed as a package with
pip `--editable` (`-e`),
so you could include a line like the following in your `requirements.txt`:

```
--editable path/to/test_lib
```

That way, you could import tools like:

```python
from test_lib.util import TestUnit
```

Sadly, that functionality is broken in newer versions of pip.
See [here][pip_broken_editable] for more info on that problem.

I've still included the `-e` in requirements files.
But you may need to add the test lib to the `PYTHONPATH`:

```shell
export PYTHONPATH="${PYTHONPATH}:path/to/test_lib"
```

[pip_broken_editable]: https://github.com/microsoft/vscode-python/issues/14570
[categorical_syllogisms_api_tests]: https://gitlab.com/momosa/categorical_syllogisms/-/tree/master/tests/api?ref_type=heads
