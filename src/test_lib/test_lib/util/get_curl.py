"""
get_curl.py

get_curl()
Get a curl command given request kwargs
"""

import json


def get_curl(request_kwargs, method='GET'):
    """
    Get a curl command for API request info
    """

    url = request_kwargs['url']
    data = request_kwargs.get('json')
    method = method.upper()

    cmd = ['curl']
    cmd.append(f'--request {method}')
    if data:
        data_str = json.dumps(data)
        cmd.append(f"--data '{data_str}'")
    cmd.append(f"'{url}'")
    return ' '.join(cmd)
