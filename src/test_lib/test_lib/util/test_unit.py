"""
test_unit.py

Tools to streamline mocking

TestUnit

A unit test class that helps streamline mocking
"""

from unittest import TestCase, mock


class TestUnit(TestCase):
    """
    A class for unit tests that helps screamline mocking
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mock = {}


    def setup_mocks(self, namespace, to_mock):
        """
        Set up one or more mocks

        Call this in a `setUp` function.
        It will set up mocks and ensure cleanup.
        Mocks are set to the `dict` `self.mock`

        Example:

            def setUp(self):
                self.setup_mocks(
                    namespace='namespace.to.test',
                    to_mock=['a_thing', 'another_thing']
                )

                self.mock['a_thing'].return_value = 'bar'

        :param str namespace: The namespace of the module being tested
        :param list to_mock: A list of things to mock.
            Do not include the namespace.
        """
        patcher = {}
        if isinstance(to_mock, str):
            to_mock = [to_mock]

        for thing_to_mock in to_mock:
            patcher[thing_to_mock] = mock.patch(
                f"{namespace}.{thing_to_mock}",
            )

        for item_name, item_patcher in patcher.items():
            self.addCleanup(item_patcher.stop)
            self.mock[item_name] = item_patcher.start()
