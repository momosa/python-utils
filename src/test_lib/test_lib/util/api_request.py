"""
api_request.py

api_request(request_kwargs, method_name='get')
Function for making arbitrary API requests
"""

import requests

def api_request(request_kwargs, method_name='get'):
    """
    Make a request to the API

    :param dict request_kwargs: arguments for the request
    :param str method_name: the name of the http request method
    :return: request

    Move this function into an API lib if available.
    """
    method = getattr(requests, method_name)
    response = method(**request_kwargs)
    return response
