"""
api_status.py

ApiStatusError

An Exception for API status code errors
"""

from test_lib.exceptions.api import ApiError

class ApiStatusError(ApiError):
    """
    A generic error API queries

    :param str test_name: The name of the test
    :param dict request_kwargs: Characterization of the API call
    :param int actual_status: The return status of the API call
    :param int expected_status: The expected status of the API call
    :param str method: The HTTP method of the call
    """
    def __init__(self, **kwargs):
        kwargs.setdefault('method', 'GET')

        errmsg = (
            f"Expected status code {kwargs['expected_status']}, "
            f"but got {kwargs['actual_status']}"
        )

        super().__init__(
            kwargs['test_name'],
            kwargs['request_kwargs'],
            errmsg,
            kwargs['method'],
        )
