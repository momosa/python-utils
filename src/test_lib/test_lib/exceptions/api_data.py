"""
api_data.py

ApiDataError

An Exception for API response data errors
"""

import json
from test_lib.exceptions.api import ApiError

class ApiDataError(ApiError):
    """
    A generic error API response data

    :param str test_name: The name of the test
    :param dict request_kwargs: Characterization of the API call
    :param int actual_data: The response body from the API call
    :param int expected_data: The expected response body
    :param str method: The HTTP method of the call
    """
    def __init__(self, **kwargs):
        kwargs.setdefault('method', 'GET')

        hline = '-'*80
        errmsg = f"""
            Incorrect API response data:
            {hline}
            Expected:
            {json.dumps(kwargs['expected_data'])}
            {hline}
            Actual:
            {json.dumps(kwargs['actual_data'])}
        """

        super().__init__(
            kwargs['test_name'],
            kwargs['request_kwargs'],
            errmsg,
            kwargs['method'],
        )
