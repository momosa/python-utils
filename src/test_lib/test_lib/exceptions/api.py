"""
api.py

ApiError

A general class for API errors
"""

from textwrap import dedent
from test_lib.util import get_curl

class ApiError(Exception):
    """ A generic error API queries
    """
    def __init__(self, test_name, request_kwargs, errmsg, method='GET'):
        curl_cmd = get_curl(request_kwargs, method)
        hline = '='*80
        errmsg = dedent(f"""
            {hline}
            API test failed: {test_name}
            {errmsg}
            {hline}
            Request info:
            method: {method},
            body:
            {request_kwargs}
            {hline}
            Curl command:
            {curl_cmd}

        """)
        super().__init__(errmsg)
