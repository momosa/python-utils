"""
api_test_case.py

ApiTestCase

Abstract class for integration tests against an API
"""

import os
from unittest import TestCase
from retrying import retry
import requests

from test_lib.api.api_db import ApiDB
from test_lib.exceptions import ApiStatusError, ApiDataError
from test_lib.util import api_request


# Add a test case called "smoke" to the json
# to retry a smoke test up to `SMOKE_MAX_WAIT_MS`.
SMOKE_MAX_WAIT_MS = int(os.environ.get("TIMEOUT") or 10 * 1000)
def _is_api_exception(err):
    """
    Determine if the exception is API-related
    """
    return isinstance(err, (ApiStatusError, ApiDataError, requests.exceptions.ConnectionError))


class ApiTestCase(TestCase):
    """
    Class for integration tests against an API
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._database = None


    def init_db(self, test_filename):
        """
        Set test case data.
        Good to do in the setUp function.

        :param str test_filename: E.g., "my_endpoint.json"
        """
        self._database = ApiDB(test_filename)


    def run_all(self):
        """
        Run all of the tests
        """
        test_names = self._database.get_test_names()

        if 'smoke' in test_names:
            self.wait_for_smoke()
            test_names.remove('smoke')

        for test_name in test_names:
            self.run_case(test_name)


    def run_case(self, test_name):
        """
        Run a given test case

        :param str test_name: key of the test we want to kwargs for
        """
        method_name = self._database.get_request_method_name(test_name)
        request_kwargs = self._database.get_request_kwargs(test_name)
        response = api_request(request_kwargs, method_name)
        expects_data = self._database.get_expected(test_name).get('json')

        self._test_status_code(test_name, response)
        if expects_data:
            self._test_data(test_name, response)


    @retry(
        retry_on_exception=_is_api_exception,
        stop_max_delay=SMOKE_MAX_WAIT_MS,
        wait_fixed=1000,
    )
    def wait_for_smoke(self):
        """
        Retry the smoke tests up to `SMOKE_MAX_WAIT_MS`
        """
        self.run_case('smoke')


    def _test_status_code(self, test_name, response):
        """
        Test the status code in the response

        :param str test_name: key of the test we want to kwargs for
        :param response: response like that given by the requests lib
        :raises: ApiStatusError if status is not as expected
        """
        expected = self._database.get_expected(test_name)['status_code']
        if not response.status_code == expected:
            raise ApiStatusError(
                test_name=test_name,
                request_kwargs=self._database.get_request_kwargs(test_name),
                actual_status=response.status_code,
                expected_status=expected,
                method=self._database.get_request_method_name(test_name),
            )


    def _test_data(self, test_name, response):
        """
        Test the data in the response

        :param str test_name: key of the test we want to kwargs for
        :param response: response like that given by the requests lib
        :raises: ApiDataError if data is not as expected
        """
        expected = self._database.get_expected(test_name)['json']
        response_json = response.json()
        if not _unordered_eq(response_json, expected):
            raise ApiDataError(
                test_name=test_name,
                request_kwargs=self._database.get_request_kwargs(test_name),
                actual_data=response_json,
                expected_data=expected,
                method=self._database.get_request_method_name(test_name),
            )


def _unordered_eq(lhs, rhs):
    """
    Test the equality of items that are understood to be unordered
    """
    if type(lhs) != type(rhs): #pylint: disable=unidiomatic-typecheck
        equal = False
    elif isinstance(lhs, dict):
        if not sorted(lhs.keys()) == sorted(rhs.keys()):
            equal = False
        else:
            equal = all((
                _unordered_eq(lhs[key], rhs[key])
                for key in lhs
            ))
    elif isinstance(lhs, list):
        if not len(lhs) == len(rhs):
            equal = False
        else:
            equal = True
            for lhs_item in lhs:
                equal = any((
                    _unordered_eq(lhs_item, rhs_item)
                    for rhs_item in rhs
                ))
                if equal is False:
                    break
    else:
        equal = lhs == rhs
    return equal
