"""
api_db.py

ApiDB
A database for handling request data and expected responses
"""

import os
import copy
import json
from pathlib import Path

DEFAULT_TEST_SERVER = "http://localhost:8000"
DATA_DIR = Path(__file__).parents[3] / "api" / "data"

class ApiDB:
    """
    A database for handling request data and expected repsonses

    :param str test_type: name of the database to initialize.
        Corresponds to the input filename
    """

    base_url = os.environ.get("TEST_SERVER", DEFAULT_TEST_SERVER)


    def __init__(self, test_filename):
        self._database = None
        data_filename = os.path.join(DATA_DIR, test_filename)
        with open(data_filename, encoding="utf-8") as json_data:
            self._database = json.loads(json_data.read())


    def get_test_names(self):
        """
        Get the list of tests

        :return: the list of tests
        :rtype: list
        """
        return list(self._database.keys())


    def get_url(self, test_name):
        """
        Get the full url for the test

        :param str test_name: key of the test we want to kwargs for
        :return: the full url for the test
        :rtype: str
        """
        endpoint = self._database[test_name]['request_kwargs']['url']
        url = os.path.join(ApiDB.base_url, endpoint)
        return url


    def get_request_method_name(self, test_name):
        """
        Get the name of the request method for the test

        :param str test_name: key of the test we want to kwargs for
        :return: the name of the request method (lower-case)
        :rtype: str
        """
        method_name = self._database[test_name].get('method', 'get')
        return method_name.lower()


    def get_request_kwargs(self, test_name):
        """
        Get the kwargs for the request method

        :param str test_name: key of the test we want to kwargs for
        :return: the kwargs for the request method
        :rtype: dict
        """
        kwargs = self._database[test_name].get('request_kwargs')
        if kwargs:
            kwargs = copy.deepcopy(kwargs)
            kwargs['url'] = self.get_url(test_name)
        return kwargs


    def get_expected(self, test_name):
        """
        Get the expected response data for the test

        :param str test_name: key of the test we want to kwargs for
        :return: the expected response data
        :rtype: dict
        """
        expected = self._database[test_name]['expected']
        return expected
