"""
mock_logging.py

Mock the logging module
"""

from dataclasses import dataclass


@dataclass
class MockLogger:
    """
    Mock the logger to just print to stdout

    Usage:

        Needs some work getting it to print the message correctly

        mock_logger = MockLogger("test_get_foo")
        self.mock["logger.debug"].return_value = \
            lambda *args: mock_logger.debug(*args)

    """

    src_name: str

    def debug(self, msg):
        """Mock debug logging as print"""
        print(f"{self.src_name}:DEBUG:{msg}")

    def info(self, msg):
        """Mock info logging as print"""
        print(f"{self.src_name}:INFO:{msg}")

    def warning(self, msg):
        """Mock warning logging as print"""
        print(f"{self.src_name}:WARNING:{msg}")

    def error(self, msg):
        """Mock error logging as print"""
        print(f"{self.src_name}:ERROR:{msg}")

    def critical(self, msg):
        """Mock critical logging as print"""
        print(f"{self.src_name}:CRITICAL:{msg}")
