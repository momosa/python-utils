"""
Test library for unit and API tests
"""
from setuptools import setup, find_packages
setup(
    name='test_lib',
    packages=find_packages(),
    description="Test library for unit and API tests",
)
