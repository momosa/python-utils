"""
file_data.py

Interact with file data efficiently while avoiding conflict.
"""

from threading import Lock
import json
import pathlib
import copy

class FileData:
    """
    FileData

    Gets data from a file, only loading the file once.
    Avoids conflicts between threads/workers.

    You can interact with objects like a read-only `dict`,
    although if you really need a `dict`, use `.asdict()`.

    Public attributes:
        N/A

    Public Classmethods:
        load(filename): load a file without creating an object
        is_loaded(filename): determine if a file is loaded
        unload(filename): unload a file
        get_n_using(filename): determine how many objects are using the file data

    Public Methods:
        get_filename(): get the filename of the data
        as_dict(): get a copy of the data as a dictionary
        str conversion and repr: handy Python standards
        read-only dict methods: provide a dict-like interface

    :param pathlib.Path filename: Location of the data
    """


    _data = {}
    _locks = {}
    _n_using = {}


    def __init__(self, filename: pathlib.Path):
        self._filename = filename
        if filename not in self._locks:
            self.load(filename)
        self._n_using[filename] += 1


    @classmethod
    def load(cls, filename):
        """
        Load data from a file

        :param pathlib.Path filename: name of the file to load
        :raises: RuntimeError if the file is not already loaded
        """
        if filename in cls._locks:
            raise RuntimeError(
                f"Attempted to load {filename} but it is already loaded"
            )
        cls._locks[filename] = Lock()
        cls._n_using[filename] = 0
        extension = filename.name.split(".")[-1].lower()
        if extension == "json":
            cls._locks[filename].acquire()
            if filename not in cls._data:
                try:
                    cls._data[filename] = _get_data_from_file(filename)
                except FileNotFoundError:
                    cls._locks[filename].release()
                    raise
            cls._locks[filename].release()
        else:
            raise NotImplementedError(
                f"Need to implement loading '{extension}' files"
            )


    @classmethod
    def is_loaded(cls, filename: pathlib.Path) -> bool:
        """
        Determine if a file is loaded

        :param pathlib.Path filename: name of the file to check on
        :return: Whether or not the file has been loaded
        :rtype: bool
        """
        return filename in cls._data


    @classmethod
    def unload(cls, filename: pathlib.Path):
        """
        Unload a file

        :param pathlib.Path filename: The name of the file to unload
        :raises: RuntimeError if the data is in use or not loaded
        """
        if filename not in cls._locks:
            raise RuntimeError(
                f"Attempted to unload file {filename}, but it is not loaded",
            )
        if cls._n_using[filename] > 0:
            raise RuntimeError(
                f"Attempted to unload file {filename}, ",
                f"but it is currently used by {cls._n_using[filename]} objects",
            )
        del cls._locks[filename]
        del cls._data[filename]
        del cls._n_using[filename]


    @classmethod
    def clear(cls):
        """
        Unload everything
        """
        cls._locks = {}
        cls._data = {}
        cls._n_using = {}


    @classmethod
    def get_n_using(cls, filename) -> int:
        """
        Get the number of objects currently using a give file

        :param pathlib.Path filename: The name of the file to check on
        :return: The number of objects using the file
        :rtype: int
        """
        return cls._n_using.get(filename, 0)


    def get_filename(self) -> pathlib.Path:
        """
        Get the filename corresponding to the object's data

        :return: The filename corresponding to the object's data
        :rtype: pathlib.Path
        """
        return self._filename


    def asdict(self):
        """Get data as a dictionary"""
        return copy.deepcopy(self._data[self._filename])


    def __del__(self):
        """Handle object deletion (does not unload)"""
        self._locks[self._filename].acquire()
        self._n_using[self._filename] = max(0, self._n_using[self._filename] - 1)
        self._locks[self._filename].release()


    def __str__(self):
        return json.dumps(self._data[self._filename])


    def __repr__(self):
        return f"FileData(filename={repr(self._filename)})"


    def __iter__(self):
        """dict-like iterability"""
        return self._data[self._filename].__iter__()


    def __getitem__(self, key):
        """dict-like bracket notation"""
        return self._data[self._filename][key]


    def get(self, key, default=None):
        """dict-like get()"""
        return self._data[self._filename].get(key, default)


    def keys(self):
        """dict-like keys()"""
        return self._data[self._filename].keys()


    def items(self):
        """dict-like items()"""
        return self._data[self._filename].items()


    def values(self):
        """dict-like values()"""
        return self._data[self._filename].values()


def _get_data_from_file(filename) -> dict: # pragma: no cover
    """
    Get data from the file.
    This method is isolated for easier mocking.

    :return: Data from the file in a usable form
    :rtype: dict
    :raises: FileNotFoundError if the file or directory does not exist
    """
    with open(filename, "r", encoding="utf-8") as file:
        data = json.load(file)
    return data
