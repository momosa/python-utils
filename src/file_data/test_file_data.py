"""
Tests for the API v1 FileData util
"""

from pathlib import Path

from test_lib.util import TestUnit

from .file_data import FileData

################################################################################
# Superclass


class TestFileData(TestUnit):
    """
    Superclass for testing FileData
    """

    def setUp(self):
        print(dir())
        self.setup_mocks(
            namespace="file_data.file_data",
            to_mock=(
                "_get_data_from_file",
            ),
        )

        self.mock["_get_data_from_file"].return_value = {"mock": "data"}


    def tearDown(self):
        super().tearDown()
        FileData.clear()


################################################################################
# File IO


class TestFileIO(TestUnit):
    """Ensure that errors from file IO will bubble up"""


    def test_no_file(self):
        """Ensure that an error is raised when the file does not exist"""
        bad_filename = Path("/") / "does_not_exist.json"
        self.assertRaises(
            FileNotFoundError,
            FileData,
            bad_filename,
        )


################################################################################
# Filetypes

class TestFiletype(TestFileData):
    """Test loading different filetypes"""


    def tearDown(self):
        super().tearDown()
        FileData.clear()


    def test_recognized_filetype(self):
        """Ensure we can load a json"""
        filename = Path("/") / "mock_filename.json"
        FileData.load(filename)


    def test_unimplemented_filetype(self):
        """Ensure an error is raised for an unimplemented filetype"""
        filename = Path("/") / "mock_filename.bad_extension"
        self.assertRaises(
            NotImplementedError,
            FileData.load,
            filename,
        )


################################################################################
# Loading and Unloading

class TestLoading(TestFileData):
    """Test determining if a file is loaded"""


    def test_positive(self):
        """Ensure we can tell that a loaded file is loaded"""
        filename = Path("/") / "mock_filename.json"
        FileData.load(filename)
        actual = FileData.is_loaded(filename)
        self.assertTrue(actual)


    def test_negative(self):
        """Ensure we can tell that a non-loaded file is not loaded"""
        loaded_filename = Path("/") / "loaded_filename.json"
        unloaded_filename = Path("/") / "unloaded_filename.json"
        FileData.load(loaded_filename)
        actual = FileData.is_loaded(unloaded_filename)
        self.assertFalse(actual)


    def test_loading_already_loaded(self):
        """Ensure load raises an error if the file is already loaded"""
        filename = Path("/") / "mock_filename.json"
        FileData.load(filename)
        self.assertRaises(
            RuntimeError,
            FileData.load,
            filename,
        )


    def test_unload(self):
        """Ensure we can unload a loaded file"""
        filename = Path("/") / "mock_filename.json"
        FileData.load(filename)
        FileData.unload(filename)
        self.assertFalse(FileData.is_loaded(filename))


    def test_unload_non_loaded(self):
        """Ensure unload raises an error if the file is not loaded"""
        filename = Path("/") / "not_loaded.json"
        self.assertRaises(
            RuntimeError,
            FileData.unload,
            filename,
        )


    def test_unload_in_use(self):
        """Ensure unload raises an error if the file is in use"""
        filename = Path("/") / "not_loaded.json"
        dummy = FileData(filename)
        self.assertRaises(
            RuntimeError,
            FileData.unload,
            filename,
        )


################################################################################
# File usage

class TestGetNumberUsing(TestFileData):
    """Test determining the number of objects using a file"""


    def test_nothing_loaded(self):
        """Ensure we can tell that nobody is using a file when nothing has ever been loaded"""
        filename = Path("/") / "mock_filename.json"
        actual = FileData.get_n_using(filename)
        expected = 0
        self.assertEqual(actual, expected)


    def test_single_object(self):
        """Ensure we can tell when a single object is using a file"""
        filename = Path("/") / "filename.json"
        other_filename = Path("/") / "other_filename.json"
        dummy = FileData(filename)
        dummy2 = FileData(other_filename)
        actual = FileData.get_n_using(filename)
        expected = 1
        self.assertEqual(actual, expected)


    def test_multiple_objects(self):
        """Ensure we can tell when multiple objects are using a file"""
        filename = Path("/") / "filename.json"
        dummy = FileData(filename)
        dummy2 = FileData(filename)
        dummy3 = FileData(filename)
        actual = FileData.get_n_using(filename)
        expected = 3
        self.assertEqual(actual, expected)


    def test_unused_loaded(self):
        """Ensure we can tell that nobody is using a loaded file"""
        filename = Path("/") / "filename.json"
        FileData.load(filename)
        actual = FileData.get_n_using(filename)
        expected = 0
        self.assertEqual(actual, expected)


    def test_deleted(self):
        """Ensure we the number of file users decreseases on object deletion"""
        filename = Path("/") / "filename.json"
        dummy = FileData(filename)
        dummy2 = FileData(filename)
        del dummy2
        actual = FileData.get_n_using(filename)
        expected = 1
        self.assertEqual(actual, expected)


################################################################################
# Get filename


class TestGetFilename(TestFileData):
    """Test getting the filename"""


    def test_get_filename(self):
        """Test getting the filename"""
        filename = Path("/") / "path" / "to" / "filename.json"
        file_data = FileData(filename)
        actual = file_data.get_filename()
        expected = Path("/") / "path" / "to" / "filename.json"
        self.assertEqual(actual, expected)


################################################################################
# As dictionary


class TestAsDict(TestFileData):
    """Test getting the data as a dictionary"""


    def test_empty_dictionary(self):
        """Ensure we get an empty dictionary when expected"""
        data = {}
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.asdict()
        expected = {}
        self.assertDictEqual(actual, expected)


    def test_regular_data(self):
        """Ensure we get the correct dictionary for normal data"""
        data = {
            "I": "promise",
            "that": {
                "I": "am",
            },
            "a": [
                "perfectly",
                "normal",
                "dictionary",
            ],
            "also": 123.45,
            "and": None,
        }
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.asdict()
        expected = {
            "I": "promise",
            "that": {
                "I": "am",
            },
            "a": [
                "perfectly",
                "normal",
                "dictionary",
            ],
            "also": 123.45,
            "and": None,
        }
        self.assertDictEqual(actual, expected)


################################################################################
# str

class TestStr(TestFileData):
    """Test getting the data as a string"""


    def test_empty_dict(self):
        """Ensure the string is correct for an empty dictionary"""
        data = {}
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = str(file_data)
        expected = "{}"
        self.assertEqual(actual, expected)


    def test_regular(self):
        """Ensure the string is correct for a pretty regular dictionary"""
        data = {
            "I": "promise",
            "that": {
                "I": "am",
            },
            "a": [
                "perfectly",
                "normal",
                "dictionary",
            ],
            "also": 123.45,
            "and": None,
        }
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = str(file_data)
        expected = (
            '{"I": "promise", "that": {"I": "am"}, '
            '"a": ["perfectly", "normal", "dictionary"], '
            '"also": 123.45, "and": null}'
        )
        self.assertEqual(actual, expected)


################################################################################
# repr

class TestRepr(TestFileData):
    """Test getting the data as a repr"""


    def test_regular(self):
        """Test a typical repr"""
        filename = Path("/") / "path" / "to" / "filename.json"
        file_data = FileData(filename)
        actual = repr(file_data)
        expected = "FileData(filename=PosixPath('/path/to/filename.json'))"
        self.assertEqual(actual, expected)


################################################################################
# Dictionary Ops

class TestDictOps(TestFileData):
    """Test read-only dictionary operations"""


    def test_iter(self):
        """Test iterability"""
        data = {"first": 1, "second": "second_value"}
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual_keys = [key for key in file_data] #pylint: disable=unnecessary-comprehension
        expected = ["first", "second"]
        self.assertCountEqual(actual_keys, expected)


    def test_getitem(self):
        """Test getting an item using bracket notation"""
        data = {
            "target": "correct",
            "distraction": "incorrect",
            "also bad": -1,
            "snek": None,
        }
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data["target"]
        expected = "correct"
        self.assertEqual(actual, expected)


    def test_get(self):
        """Test getting an item that's there with .get"""
        data = {
            "distraction": "incorrect",
            "target": "correct",
            "also bad": -1,
            "snek": None,
        }
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.get("target")
        expected = "correct"
        self.assertEqual(actual, expected)


    def test_get_default(self):
        """Test specifying a default value"""
        data = {
            "distraction": "incorrect",
            "target": "correct",
            "also bad": -1,
            "snek": None,
        }
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.get("not there", 42)
        expected = 42
        self.assertEqual(actual, expected)


    def test_get_default_default(self):
        """Test the default value for the default param for get"""
        data = {
            "distraction": "incorrect",
            "target": "correct",
            "also bad": -1,
            "snek": None,
        }
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.get("not there")
        expected = None
        self.assertEqual(actual, expected)


    def test_keys(self):
        """Test getting the keys"""
        data = {"first": 1, "second": "second_value"}
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.keys()
        expected = ("first", "second")
        self.assertCountEqual(actual, expected)


    def test_items(self):
        """Test getting the items"""
        data = {"first": 1, "second": "second_value"}
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.items()
        expected = (("first", 1), ("second", "second_value"))
        self.assertCountEqual(actual, expected)


    def test_values(self):
        """Test getting the values"""
        data = {"first": 1, "second": "second_value"}
        self.mock["_get_data_from_file"].return_value = data
        filename = Path("/") / "mock_filename.json"
        file_data = FileData(filename)
        actual = file_data.values()
        expected = (1, "second_value")
        self.assertCountEqual(actual, expected)
