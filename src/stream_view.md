# Stream a data in and out

This is useful in dealing with very large files stored in a secure location.
The client can access your endpoint, but not the file directly.
The endpoint will stream the file as it comes in.
Your API server is basically working as a proxy.

In many cases there are better solutions, such as letting nginx serve the file.
But just in case you don't have that setup...

```python
import requests
from django.http import StreamingHttpResponse
from rest_framework.views import APIView

class MyView(APIView):
    def get(self, request):
        filename = "foo.zip"
        source_url = "https://source_file.zip"
        return StreamingHttpResponse(
            _stream_data(source_url),
            content_type="application/zip",
            headers={"Content-Disposition": f'attachment; filename="{filename}"'},
        )

def _stream_data(source_url):
    """Stream data as it becomes available"""
    with requests.get(source_url, stream=True) as response:
        for chunk in response.iter_content():
            yield chunk
```
