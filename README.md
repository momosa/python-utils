# Python Utils

Various little python toys.

1. [Development](#development)

    1.1. [Dev setup for this repo](#dev_setup)

    1.2. [Running tests](#running_tests)

1. [File Data Lib](src/file_data/file_data.py)

1. [Test Lib](src/test_lib/README.md)

- - -

<a name="development"></a>

## 1\. Development

<a name="dev_setup"></a>

### 1.1\. Dev setup for this repo

First-time setup up:
set up your virtual env.
The following steps are for `pyenv`.
See the [pyenv installation docs][pyenv_install_docs]
for instructions on installing `pyenv`.

```shell
pyenv install 3.10
```

Create the virtual environment from the project directory:

```shell
pyenv virtualenv 3.10 python-utils
```

Now you can activate the virtual environment:

```shell
pyenv activate python-utils
```

With the virtual environment activated,
install the dependencies from the project dir:

```shell
pip install -r requirements.txt
```

You can make sure everything works by running tests:

```shell
bin/test.sh all
```

<a name="running_tests"></a>

### 1.2\. Running tests

```shell
bin/test.sh -h
```

[pyenv_install_docs]: https://github.com/pyenv/pyenv#installation "Pyenv install docs"
